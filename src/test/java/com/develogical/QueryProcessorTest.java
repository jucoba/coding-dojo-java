package com.develogical;

import org.junit.Test;

import java.util.Map;

import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;

public class QueryProcessorTest {

    @Test
    public void canGreetYou() {
        String result = new QueryProcessor().process("hi");
        assertNotNull(result);
        assertThat(result, is("hello"));
    }
    
    @Test
    public void askForName() {
    	String result = new QueryProcessor().process("what is your name");
        assertNotNull(result);
        assertThat(result, is("coyote"));
    	
    }
    
    @Test
    public void sumar20y2() {
    	String result = new QueryProcessor().process("qeey: what is 20 plus 2");
        assertNotNull(result);
        assertThat(result, is("22"));
    }
    
    @Test
    public void sumar6y13() {
    	String result = new QueryProcessor().process("frerr: what is 6 plus 13");
        assertNotNull(result);
        assertThat(result, is("19"));
    }

    @Test
    public void returnsEmptyStringForUnknownQueries() {
        String result = new QueryProcessor().process("unknown");
        assertNotNull(result);
        assertThat(result, is(""));
    }
    
    @Test
    public void largestNumber_19_78_737_982() {
    	String result = new QueryProcessor().process("erty: which of the following numbers is the largest: 19, 78, 737, 982");
        assertNotNull(result);
        assertThat(result, is("982"));
    }
    
    //2e4ad2b0: which of the following numbers is the largest: 157, 82
    @Test
    public void largestNumber_157_82() {
    	String result = new QueryProcessor().process("2e4ad2b0: which of the following numbers is the largest: 157, 82");    	
        assertNotNull(result);
        assertThat(result, is("157"));
    }
    
    @Test
    public void minorNumber_19_78_737_982() {
    	String result = new QueryProcessor().process("erty: which of the following numbers is the minor: 19, 78, 737, 982");
        assertNotNull(result);
        assertThat(result, is("19"));
    }
    
    @Test
    public void multiply_5_19() {
    	String result = new QueryProcessor().process("f6319f90: what is 5 multiplied by 19");
        assertNotNull(result);
        assertThat(result, is("95"));
    }
    
    @Test
    public void square_and_a_cube_64_4096() {
    	String result = new QueryProcessor().process("6669e970: which of the following numbers is both a square and a cube: 64, 4096");
        assertNotNull(result);
        assertThat(result, is("64, 4096"));
    }
    
    @Test
    public void square_and_a_cube_565_2401_1369_303() {
    	String result = new QueryProcessor().process("4cc0a7a0: which of the following numbers is both a square and a cube: 565, 2401, 1369, 303");
        assertNotNull(result);
        assertThat(result, is(""));
    }
    
    
    
    //

    
    //which of the following numbers is the largest: 19, 78, 737, 982

    
    
}



