package com.develogical;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.swing.event.InternalFrameEvent;

public class QueryProcessor {

    public String process(String query) {
    	System.out.println("Query !! "+query);
    	String respuesta = "";
        if (query.contains("hi")) {
            respuesta = "hello";
        }
        if (query.contains("square and a cube"))
        {
        	respuesta = cuadrado_cubo(query);
        }
        if (query.contains("plus"))
        {
        	respuesta = sumar(query); 
        }
        if (query.contains("largest"))
        {
        	respuesta = mayor(query); 
        }
        if (query.contains("multiplied"))
        {
        	respuesta = multiplicar(query); 
        }
        
        
        if (query.contains("minor"))
        {
        	respuesta = menor(query); 
        }        
        if (query.contains("name"))
        {
        	respuesta = "coyote";
        }        
        
        return respuesta;
    }
    
    
    
    private String sumar(String query) {
    	String[] palabras = query.split(" ");
    	String stNum1 = palabras[3];
    	String stNum2 = palabras[5];
    	int num1 = Integer.parseInt(stNum1);
    	int num2 = Integer.parseInt(stNum2);
    	Integer resultado = num1 + num2;
    	return resultado.toString();
    }
    
    private String multiplicar(String query) {
    	String[] palabras = query.split(" ");
    	String stNum1 = palabras[3];
    	String stNum2 = palabras[6];
    	int num1 = Integer.parseInt(stNum1);
    	int num2 = Integer.parseInt(stNum2);
    	Integer resultado = num1 * num2;
    	return resultado.toString();
    }
    
    private ArrayList<Integer> obtenerNumerosOrdenados(String query)
    {
    	String[] palabras = query.split(":");
    	String[] stNumeros = palabras[2].split(",");
    	
    	
    	ArrayList<Integer> numeros = new ArrayList<Integer>();
    	
    	
    	for (String stNumer : stNumeros )
    	{
    		numeros.add(Integer.parseInt(stNumer.trim()));
    	}
    	Collections.sort(numeros);
    	return numeros;
    }
    
    private String mayor(String query) {
    	
    	ArrayList<Integer> numeros = obtenerNumerosOrdenados(query);
    	
    	//System.out.println("Numeros: "+numeros.get(num).toString());
    	return numeros.get(numeros.size() - 1).toString();
    	
    }
    
    private String cuadrado_cubo(String query) {
    	ArrayList<Integer> numeros = obtenerNumerosOrdenados(query);
    	StringBuilder numerosCumple = new StringBuilder(); 
    	
    	
    	for (Integer numero : numeros) {
    		Double cuadrado = Math.sqrt(numero);
    		Double cubo = Math.cbrt(numero);
    		 
    		if ( (cuadrado - cuadrado.intValue() == 0) && 
    				(cubo - cubo.intValue() == 0) ) {
    			numerosCumple.append(numero);
    			numerosCumple.append(", ");
    		}
    		
    	}
    	String respuesta = numerosCumple.toString();
    	if (respuesta.trim().equals(""))
    	{
    		return respuesta;
    	}
    	return respuesta.substring(0,respuesta.length()-2);
    	
    }
    
    
    private String menor(String query) {
    	
    	ArrayList<Integer> numeros = obtenerNumerosOrdenados(query);
    	//System.out.println("Numeros: "+numeros.get(num).toString());
    	return numeros.get(0).toString();
    	
    }
    
    
    
    
    
    
    
    

}
